import axios from "axios";

const BASE_URL = "http://localhost/back_mairie/public/index.php/api/events";

async function getEvents() {
  return await axios.get(BASE_URL);
}

async function getEvent(id) {
  return await axios.get(BASE_URL + "/" + id);
}

async function postEvent(data) {
  return await axios.post(BASE_URL, data);
}

async function putEvent(id, data) {
  return await axios.put(BASE_URL + "/" + id, data);
}

async function deleteEvent(id) {
  return await axios.delete(BASE_URL + "/" + id);
}

export { getEvents, getEvent, postEvent, putEvent, deleteEvent };
