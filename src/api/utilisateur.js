import axios from "axios";
import router from "../router";

const BASE_URL = "http://localhost/back_mairie/public/index.php/";

async function createUser(email, mdp, prenom, nom) {
  return await axios.post(BASE_URL + "auth/register", {
    email: email,
    password: mdp,
    firstname: prenom,
    lastname: nom,
  });
}

async function connectUser(email, mdp) {
  return await axios
    .post(BASE_URL + "auth/login", { email: email, password: mdp })
    .then((resp) => {
      console.log(resp);
      const token = resp.data.token;
      const userData = atob(resp.data.token.split(".")[1]); // on récupère les données de l'utilisateur, par défaut, login, rôles
      localStorage.setItem("firstname", userData.firstname); // store the user in localstorage
      localStorage.setItem("lastname", userData.lastname); // store the user in localstorage
      localStorage.setItem("email", userData.email); // store the user in localstorage
      localStorage.setItem("usertoken", token); // store the token in localstorage
      router.push("/");
      console.info("CONNEXION SUCCESS");
    })
    .catch((err) => {
      console.log(err);
      localStorage.removeItem("usertoken"); // if the request fails, remove any possible user token if possible
    });
}

async function disconnectUser() {
  localStorage.removeItem("usertoken"); // if the request fails, remove any possible user token if possible
}

export { createUser, connectUser, disconnectUser };
