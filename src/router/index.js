import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },

  {
    path: "/modifier/:id",
    name: "ModifierEvenement",
    component: () =>
      import(
        /* webpackChunkName: "modifier" */ "../views/ModifierEvenement.vue"
      ),
  },
  {
    path: "/modifier",
    name: "Modifier_old",
    component: () =>
      import(
        /* webpackChunkName: "modifier" */ "../views/ModifierEvenement.vue"
      ),
  },
  {
    path: "/supprimer/:id",
    name: "supprimer_evenement",
    component: () =>
      import(
        /* webpackChunkName: "modifier" */ "../views/SupprimerEvenement.vue"
      ),
  },
  {
    path: "/ajouter",
    name: "AjouterEvenement",
    component: () =>
      import(/* webpackChunkName: "ajouter" */ "../views/AjouterEvenement.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/SeConnecter.vue"),
  },
  {
    path: "/logout",
    name: "Logout",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/SeDeconnecter.vue"),
  },
  {
    path: "/register",
    name: "Register",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/CreerUser.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
